package org.ludo;
public class Traitement {
    private Traitement(){}
    public static int recherche(String phrase, char lettre) {
        /*
          Fonction qui prends en paramètre une phrase  et une lettre .
          Elle va chercher la lettre dans la phrase et retourner la position de la lettre
         */
        int nombreCaractere = 0;
        boolean dedans = false ;
        for (int i = 0; i < phrase.length(); i++) {
            char caractere = phrase.charAt(i);
            if (lettre != caractere) {
                nombreCaractere++;
            }else{
                nombreCaractere++;
                dedans = true;
                break;
            }
        }
        // On vérifie si le caractère est bien dans la phrase
        if(dedans) return nombreCaractere;
        else return 0;
    }
}