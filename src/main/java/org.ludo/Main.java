package org.ludo;

import java.util.Scanner;
import static org.ludo.Traitement.recherche;

public class Main {
    public static void main(String[] args) {
        // Initialisation des input
        Scanner myObj = new Scanner(System.in);
        // Création du input pour récupérer une phrase
        System.out.println("Entrez votre phrase:");
        String phrase = myObj.nextLine();
        // Création du input pour récupérer un caractère
        System.out.println("Entrez une lettre:");
        char lettre = myObj.next().charAt(0);

        // Appel de la fonction recherche de la classe Traitement
        int reponse = recherche(phrase, lettre);

        if (reponse == 0) {
            System.out.println("La lettre"+lettre+" n'est pas présente dans la phrase.");
        }else if(reponse == 1) {
            System.out.println("La lettre "+lettre+" est en "+reponse+" ère position.");
        }else{
            System.out.println("La lettre "+lettre+" est en "+reponse+" ième position.");
        }
    }
}